package proj;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import mockit.Deencapsulation;

public class WebCrawlerTest
{
    WebCrawler webCrawler = new WebCrawler();

    @Test
    public void testConvertJsonToPojo()
    {
        ClassLoader classLoader = webCrawler.getClass().getClassLoader();
        File file = new File( classLoader.getResource( "internet.json" ).getFile() );
        Input inp = Deencapsulation.invoke( WebCrawler.class, "convertJsonToPojo", file );
        Assert.assertNotNull( inp );
    }

    @Test( expected = NullPointerException.class )
    public void testPrintResp()
    {
        Map<String, Integer> map = new HashMap<>();
        List<String> list = null;

        webCrawler.printResp( map, list );
        Assert.fail();
    }

    @Test( expected = NullPointerException.class )
    public void testCrawlPagesCase1()
    {
        Map<String, Integer> map = new HashMap<>();
        List<String> list = null;
        String sPage = "";
        webCrawler.crawlPages( null, list, map, sPage );
        Assert.fail();
    }

    @Test
    public void testCrawlPagesCase2()
    {
        Map<String, Integer> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        String sPage = "page-00";
        InputPages inpPages = new InputPages();
        inpPages.setAddress( "page-00" );
        List<InputPages> inpPagesList = new ArrayList<>();
        Input inp = new Input();
        inp.setPages( inpPagesList );

        webCrawler.crawlPages( inp, list, map, sPage );
    }
}