package proj;

public class WebCrawlerException extends RuntimeException
{
    /**
     * 
     */
    private static final long serialVersionUID = 4576299208606335961L;

    private String errorMsg;
    private String stackTraceMsg;

    public String getErrorMsg()
    {
        return errorMsg;
    }

    public void setErrorMsg( String errorMsg )
    {
        this.errorMsg = errorMsg;
    }

    public String getStackTraceMsg()
    {
        return stackTraceMsg;
    }

    public void setStackTraceMsg( String stackTraceMsg )
    {
        this.stackTraceMsg = stackTraceMsg;
    }

    public WebCrawlerException( String errorMessage,
        String stackTraceMessage )
    {
        this.errorMsg = errorMessage;
        this.stackTraceMsg = stackTraceMessage;

    }

}
