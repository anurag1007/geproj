package proj;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.ObjectMapper;

public class WebCrawler
{
    public static void main( String[] args ) throws Exception
    {
        String startPage = fetchInput();
        WebCrawler webCraw = new WebCrawler();
        ClassLoader classLoader = webCraw.getClass().getClassLoader();
        File file = new File( classLoader.getResource( "internet.json" ).getFile() );

        if( startPage.isEmpty() )
            throw new WebCrawlerException( "invalid input", "" );

        Input inpJson = convertJsonToPojo( file );
        if( inpJson == null )
            throw new WebCrawlerException( "empty input", "" );

        webCraw.performPageIteration( inpJson, startPage );
    }

    private void performPageIteration( Input inpJson,
                                       String startPage ) throws InterruptedException
    {
        Map<String, Integer> successMap = new HashMap<>();
        List<String> errorPages = new ArrayList<>();

        ExecutorService exec = Executors.newCachedThreadPool();
        crawlPages( inpJson, errorPages, successMap, startPage, exec );
        Thread.sleep( 100 );
        exec.shutdown();
        exec.awaitTermination( 100, TimeUnit.MILLISECONDS );
        printResp( successMap, errorPages );
    }

    public void crawlPages( Input inpJson,
                            List<String> errorPages,
                            Map<String, Integer> successMap,
                            String startPage,
                            ExecutorService exec )
    {
        Optional<InputPages> inpPageOpt = inpJson.getPages().stream().filter( p -> p.getAddress().equals( startPage ) ).findAny();
        if( inpPageOpt.isPresent() )
        {
            successMap.put( startPage, successMap.containsKey( startPage ) ? successMap.get( startPage )
                : 1 );

            List<String> inpLinks = inpPageOpt.get().getLinks();

            inpLinks.stream().forEach( pageLink -> {
                successMap.put( pageLink, successMap.containsKey( pageLink ) ? 2
                    : 1 );

                if( successMap.get( pageLink ) != 2 )
                    exec.submit( () -> crawlPages( inpJson, errorPages, successMap, pageLink, exec ), true );
            } );
        }
        else
        {
            if( successMap.isEmpty() )
                successMap.put( startPage, 0 );
            else
                successMap.put( startPage, successMap.get( startPage ) == 2 ? 3
                    : 0 );
            errorPages.add( startPage );
        }
    }

    private static Input convertJsonToPojo( File file ) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue( file, Input.class );
    }

    private static String fetchInput()
    {
        Scanner sc = new Scanner( System.in );
        System.out.println( "enter the start page:" );
        String inpLink = sc.nextLine();
        sc.close();
        return inpLink;
    }

    public void printResp( Map<String, Integer> successMap,
                           List<String> errorPages )
    {
        final StringBuilder successSb = new StringBuilder( "[" );
        final StringBuilder skippkedSb = new StringBuilder( "[" );
        final StringBuilder errorSb = new StringBuilder( "[" );

        successMap.entrySet().stream().forEach( m -> {
            if( m.getValue() == 1 )
                successSb.append( m.getKey() ).append( " " );
            else if( m.getValue() == 2 )
            {
                skippkedSb.append( m.getKey() ).append( " " );
                successSb.append( m.getKey() ).append( " " );
            }
            else if( m.getValue() == 3 )
                skippkedSb.append( m.getKey() ).append( " " );
        } );

        errorPages.stream().forEach( p -> errorSb.append( p ).append( " " ) );

        System.out.println( "Success: \n" + successSb.toString() + "]" );
        System.out.println( "Skipped: \n" + skippkedSb.toString() + "]" );
        System.out.println( "Error: \n" + errorSb.toString() + "]" );
    }
}