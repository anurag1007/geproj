package proj;

import java.util.List;

public class Input
{
    List<InputPages> pages;

    public List<InputPages> getPages()
    {
        return pages;
    }

    public void setPages( List<InputPages> pages )
    {
        this.pages = pages;
    }
}